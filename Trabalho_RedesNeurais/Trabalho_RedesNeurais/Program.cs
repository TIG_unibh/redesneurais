﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trabalho_RedesNeurais
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\nIniciando Rede Neural para alunos\n");
            List<Aluno> ListaAluno = new List<Aluno>();
            using (var reader = new StreamReader(@"AnaliseEstudo.csv"))
            {
                reader.ReadLine();
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');

                    //Console.WriteLine(line);
                    //Console.ReadLine();
                    int Aprovado = 0;
                    int soma = Convert.ToInt32(values[3])+ Convert.ToInt32(values[4])+ Convert.ToInt32(values[5]) /3;
                    if (soma >= 36)
                    {
                        Aprovado = 1;
                    }

                    Aluno aluno = new Aluno(Convert.ToInt32(values[0]), Convert.ToInt32(values[1]), Convert.ToInt32(values[2]), Convert.ToInt32(values[3]), Convert.ToInt32(values[4]), Convert.ToInt32(values[5]),Aprovado);
                    ListaAluno.Add(aluno);
                }
                reader.ReadToEnd();
                //Console.ReadLine();
            }

            Console.WriteLine("Criando uma rede neural de  3 camadas");
            int numInput = 3;
            int[] numHidden = new int[] { 5, 6, 10 }; 
            int numOutput = 1;
            int count = 0;
            DeepNet dn = new DeepNet(numInput, numHidden, numOutput);

            int nw = DeepNet.NumWeights(numInput, numHidden, numOutput);
            Console.WriteLine("Setting weights and biases to 0.01 to " + (nw / 100.0).ToString("F2"));
            double[] wts = new double[nw];
            for (int i = 0; i < wts.Length; ++i)
                wts[i] = (i + 1) * 0.01;  
            dn.SetWeights(wts);  

            Console.WriteLine("\nComputando valores recebidos\n");
            foreach (Aluno aluno in ListaAluno)
            {
                double[] xValues = new double[] { aluno.Idade, aluno.TempoEstudo, aluno.Faltas };
                dn.ComputeOutputs(xValues);
                dn.Dump(true, ListaAluno, count);  
                count++;
            }

            CriarArquviTXT(ListaAluno);

            Console.WriteLine("\nFim da rede \n");
            Console.ReadLine();
        }

        static void CriarArquviTXT(List<Aluno> ListaALuno)
        {
            string path = @"PrevisaoAlunos.txt";
            if (!File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    int count = 1;
                    foreach (Aluno aluno in ListaALuno)
                    {
                        String Previsao, ResultadoFinal;
                        if(aluno.Previsao == 1)
                        {
                            Previsao = "Aprovado";
                        }
                        else
                        {
                            Previsao = "Reprovado";
                        }

                        if (aluno.Aprovado == 1)
                        {
                            ResultadoFinal = "Aprovado";
                        }
                        else
                        {
                            ResultadoFinal = "Reprovado";
                        }

                        sw.WriteLine("Aluno ("+ count +")");
                        sw.WriteLine("Idade: "+ aluno.Idade);
                        sw.WriteLine("Tempo De Estudo: "+ aluno.TempoEstudo);
                        sw.WriteLine("Faltas: "+ aluno.Faltas);
                        sw.WriteLine("Prova 1: "+ aluno.Prova1);
                        sw.WriteLine("Prova 2: "+ aluno.Prova2);
                        sw.WriteLine("Prova 3: "+ aluno.Prova3);
                        sw.WriteLine("Previsao: " + Previsao);
                        sw.WriteLine("Resultado Final: "+ ResultadoFinal);
                        sw.WriteLine("\n------------------------------------------");
                    }
                }
            }

        }
    }
}
