﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trabalho_RedesNeurais
{
    public class Aluno
    {
        int idade;
        int tempoEstudo;
        int faltas;
        int prova1;
        int prova2;
        int prova3;
        int aprovado;
        double previsao;

        public Aluno(int idade, int tempoEstudo, int faltas, int prova1, int prova2, int prova3, int aprovado)
        {
            this.idade = idade;
            this.tempoEstudo = tempoEstudo;
            this.faltas = faltas;
            this.prova1 = prova1;
            this.prova2 = prova2;
            this.prova3 = prova3;
            this.aprovado = aprovado;
        }

        public int Idade
        {
            get
            {
                return idade;
            }

            set
            {
                idade = value;
            }
        }

        public int TempoEstudo
        {
            get
            {
                return tempoEstudo;
            }

            set
            {
                tempoEstudo = value;
            }
        }

        public int Faltas
        {
            get
            {
                return faltas;
            }

            set
            {
                faltas = value;
            }
        }

        public int Prova1
        {
            get
            {
                return prova1;
            }

            set
            {
                prova1 = value;
            }
        }

        public int Prova2
        {
            get
            {
                return prova2;
            }

            set
            {
                prova2 = value;
            }
        }

        public int Prova3
        {
            get
            {
                return prova3;
            }

            set
            {
                prova3 = value;
            }
        }

        public int Aprovado
        {
            get
            {
                return aprovado;
            }

            set
            {
                aprovado = value;
            }
        }

        public double Previsao
        {
            get
            {
                return previsao;
            }

            set
            {
                previsao = value;
            }
        }
    }
}
