{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from sklearn.linear_model import perceptron"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Dados de treino\n",
    "# Conjunto de dados que representa a operação OR\n",
    "x = np.array([[18,17,15],[2,2,2],[4,2,6]])\n",
    "\n",
    "# Representa o resultado da operação AND entre X1  e X2\n",
    "y = np.array([0,0,1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "###Rotaciona os Dados\n",
    "x = np.rot90(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Perceptron(alpha=0.0001, class_weight=None, eta0=0.1, fit_intercept=True,\n",
       "      max_iter=1000, n_iter=None, n_jobs=1, penalty=None, random_state=0,\n",
       "      shuffle=True, tol=None, verbose=0, warm_start=False)"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "##Criando o modelo de predição\n",
    "## max_iter = Quantidade máxima de épocas as serem utilizadas\n",
    "## eta = taxa de aprendizagem\n",
    "net = perceptron.Perceptron(max_iter=1000,eta0=0.1)\n",
    "net.fit(x, y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Saída Esperada  [0 0 1]\n",
      "Saída Atual     [0 0 1]\n",
      "Precisão        100.0%\n",
      "Pesos: [[  1.8 -12.   -0.4]]\n"
     ]
    }
   ],
   "source": [
    "####Resultado da etapa de treino e teste\n",
    "\n",
    "print (\"Saída Esperada  \" + str(net.predict(x)))\n",
    "print (\"Saída Atual     \" + str(y))\n",
    "print (\"Precisão        \" + str(net.score(x, y) * 100) + \"%\")\n",
    "print('Pesos: ' + str(net.coef_))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
